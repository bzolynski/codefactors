<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\DecoratorPattern\Decorators;

use CODEfactors\DesignPatterns\DecoratorPattern\Products\ProductInterface;

abstract class PriceDecorator implements ProductInterface
{
    public abstract function price(): float;
}
