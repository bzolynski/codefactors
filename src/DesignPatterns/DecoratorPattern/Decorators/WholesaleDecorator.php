<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\DecoratorPattern\Decorators;

use CODEfactors\DesignPatterns\DecoratorPattern\Products\ProductInterface;

class WholesaleDecorator extends PriceDecorator
{
    private $product;

    private $amount;

    public function __construct(ProductInterface $product, int $amount)
    {
        $this->product = $product;
        $this->setAmount($amount);
    }

    public function price(): float
    {
        return $this->product->price() - $this->priceReduction();
    }

    private function setAmount(int $amount)
    {
        if ($amount > 5000) {
            $amount = 5000;
        }
        $this->amount = $amount;
    }

    private function priceReduction(): float
    {
        return floor($this->amount / 500);
    }
}
