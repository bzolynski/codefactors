<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\DecoratorPattern\Decorators;

use CODEfactors\DesignPatterns\DecoratorPattern\Products\ProductInterface;

class VatIncludedDecorator extends PriceDecorator
{
    private $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    public function price(): float
    {
        return ceil($this->product->price() * 1.23);
    }
}
