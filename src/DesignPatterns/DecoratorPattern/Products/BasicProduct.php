<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\DecoratorPattern\Products;

class BasicProduct implements ProductInterface
{
    const CATALOGUE_PRICE = 150.0;

    public function price(): float
    {
        return self::CATALOGUE_PRICE;
    }
}
