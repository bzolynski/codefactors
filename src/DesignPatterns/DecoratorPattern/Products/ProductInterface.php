<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\DecoratorPattern\Products;

interface ProductInterface
{
    public function price(): float;
}
