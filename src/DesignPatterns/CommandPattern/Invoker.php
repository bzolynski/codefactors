<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern;

use CODEfactors\DesignPatterns\CommandPattern\Command\CommandInterface;

class Invoker
{
    private $stack = array();

    private $commandsLog = array();

    private $commandsCount = 0;

    public function run(CommandInterface $command)
    {
        $this->logCommandAction($command);
        $command->execute();
        $this->addCommandToStack($command);
        $this->commandsCount += 1;
    }

    public function undo()
    {
        if (empty($this->commands)) {
            return;
        }
        $command = end($this->stack);
        $command->undo();
        $this->removeLastCommandFromStack();
    }

    /**
     * Alternatively to run()
     */
    public function runStack()
    {
        foreach ($this->stack as $command) {
            $this->run($command);
        }
    }

    public function getCommandsCount(): int
    {
        return $this->commandsCount;
    }

    public function addCommandToStack(CommandInterface $command)
    {
        $this->stack[] = $command;
    }

    public function getCommandsLog(): array
    {
        return $this->commandsLog;
    }

    private function removeLastCommandFromStack()
    {
        array_pop($this->stack);
    }

    private function logCommandAction(CommandInterface $command)
    {
        $this->commandsLog[] = 'Executing command: ' . get_class($command);
    }
}
