<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern\Command;

use CODEfactors\DesignPatterns\CommandPattern\Account;

class CloseTransaction implements CommandInterface
{
    private $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    public function execute()
    {
        $this->account->close();
    }

    public function undo()
    {
        $this->account->open();
    }
}
