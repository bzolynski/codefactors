<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern\Command;

interface CommandInterface
{
    public function execute();

    public function undo();
}
