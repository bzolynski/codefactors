<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern\Command;

use CODEfactors\DesignPatterns\CommandPattern\Account;

class OpenTransaction implements CommandInterface
{
    private $tokenAccount;

    public function __construct(Account $tokenAccount)
    {
        $this->tokenAccount = $tokenAccount;
    }

    public function execute()
    {
        $this->tokenAccount->open();
    }

    public function undo()
    {
        $this->tokenAccount->close();
    }
}
