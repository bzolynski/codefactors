<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern\Command;

use CODEfactors\DesignPatterns\CommandPattern\Account;

class Transfer implements CommandInterface
{
    private $tokenAccount;

    private $value;

    public function __construct(Account $tokenAccount, int $value)
    {
        $this->tokenAccount = $tokenAccount;
        $this->value = $value;
    }

    public function execute()
    {
        $this->tokenAccount->add($this->value);
    }

    public function undo()
    {
        $this->tokenAccount->remove($this->value);
    }
}
