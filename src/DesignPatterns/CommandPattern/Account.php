<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\CommandPattern;

use Exception;

class Account
{
    private $open;

    private $amount = 0;

    public function __construct()
    {
        $this->open = false;
    }

    public function open()
    {
        if ($this->isOpened()) {
            throw new Exception('Account already opened');
        }
        $this->open = true;
    }

    public function add(int $value)
    {
        $this->amount += $value;
    }

    public function remove(int $value)
    {
        $this->amount -= $value;
    }

    public function close()
    {
        $this->open = false;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    private function isOpened(): bool
    {
        return $this->open;
    }
}
