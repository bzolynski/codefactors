<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StrategyPattern;

/**
 * Strategy can be set in constructor, or a in particular method.
 * In this example, strategy is determined when pay() method is called,
 * which can be handy when we want to compare prices between different payment methods,
 * though the method naming should reflect the behaviour, e.g. payAmount().
 */
class ShoppingCart
{
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    public function addItem(Item $item): void
    {
        $this->items[] = $item;
    }

    public function getAmount(): int
    {
        $amount = 0;
        foreach ($this->items as $item) {
            /** @var $item Item */
            $amount += $item->getPrice();
        }
        return $amount;
    }

    public function pay(PaymentStrategy $paymentStrategy): string
    {
        return $paymentStrategy->pay($this->getAmount());
    }
}
