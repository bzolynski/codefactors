<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StrategyPattern;

class CreditCardStrategy implements PaymentStrategy
{
    public function __construct(string $name, string $iban, string $ccv, string $expiryDate)
    {
        // setting variables intentionally omitted
    }

    public function pay(int $amount): string
    {
        return 'Credit card payment for EUR ' . $amount;
    }
}
