<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StrategyPattern;

class Item
{
    private $name;

    private $price;

    public function __construct(string $name, int $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}
