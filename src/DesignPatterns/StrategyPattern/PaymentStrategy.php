<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StrategyPattern;

interface PaymentStrategy
{
    public function pay(int $amount): string;
}
