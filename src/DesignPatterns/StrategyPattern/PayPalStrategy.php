<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StrategyPattern;

class PayPalStrategy implements PaymentStrategy
{
    public function __construct(string $email, string $password)
    {
        // setting variables intentionally omitted
    }

    public function pay(int $amount): string
    {
        return 'PayPal payment for EUR ' . $amount;
    }
}
