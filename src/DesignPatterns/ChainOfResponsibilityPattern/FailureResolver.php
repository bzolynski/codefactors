<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers\Handler;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;

use Exception;

class FailureResolver
{
    /** @var Handler[] */
    private $chain = array();

    public function addHandler(Handler $handler)
    {
        $this->chain[] = $handler;
        $this->setSuccessor($handler);
    }

    public function resolve(FailureRequest $request): ResolvedResponse
    {
        foreach ($this->chain as $handler) {
            $handler->resolve($request);
            if ($handler->isResolved()) {
                return $handler->getResolvedResponse();
            }
        }
        throw new Exception('This failure cannot be resolved');
    }

    protected function setSuccessor(Handler $handler)
    {
        $lastHandler = count($this->chain);
        if ($lastHandler > 0) {
            $this->chain[$lastHandler - 1]->setSuccessor($handler);
        }
    }
}
