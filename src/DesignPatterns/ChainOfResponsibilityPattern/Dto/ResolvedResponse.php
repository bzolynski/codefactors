<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto;

class ResolvedResponse
{
    const THIRD_PARTY_SOLUTION = 1;

    const COMMUNICATION_DEPARTMENT_SOLUTION = 2;

    const ACCUMULATORS_DEPARTMENT_SOLUTION = 3;

    private $code = 0;

    public function __construct(int $solutionCode)
    {
        $this->code = $solutionCode;
    }

    public function getCode(): int
    {
        return $this->code;
    }
}
