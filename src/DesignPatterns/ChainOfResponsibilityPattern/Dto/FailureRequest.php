<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto;

class FailureRequest
{
    const REASONS = [
        'TRANSMITTER' => 1,
        'RECEIVER' => 2,
        'ENCODER' => 3,
        'SOLAR_BATTERY' => 4
    ];

    private $manufacturer;

    private $reason;

    public function hasManufacturer(): bool
    {
        return is_string($this->manufacturer) && $this->manufacturer;
    }

    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    public function ackTransmitter(string $manufacturer)
    {
        $this->setManufacturer($manufacturer);
        $this->setReason(self::REASONS['TRANSMITTER']);
    }

    public function isTransmitter(): bool
    {
        return $this->reason === self::REASONS['TRANSMITTER'];
    }

    public function ackReceiver(string $manufacturer)
    {
        $this->setManufacturer($manufacturer);
        $this->setReason(self::REASONS['RECEIVER']);
    }

    public function isReceiver(): bool
    {
        return $this->reason === self::REASONS['RECEIVER'];
    }

    public function ackEncoder(string $manufacturer)
    {
        $this->setManufacturer($manufacturer);
        $this->setReason(self::REASONS['ENCODER']);
    }

    public function isEncoder(): bool
    {
        return $this->reason === self::REASONS['ENCODER'];
    }

    public function ackSolarBattery(string $manufacturer)
    {
        $this->setManufacturer($manufacturer);
        $this->setReason(self::REASONS['SOLAR_BATTERY']);
    }

    public function isSolarBattery(): bool
    {
        return $this->reason === self::REASONS['SOLAR_BATTERY'];
    }

    private function setManufacturer(string $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    private function setReason(int $reason)
    {
        $this->reason = $reason;
    }
}
