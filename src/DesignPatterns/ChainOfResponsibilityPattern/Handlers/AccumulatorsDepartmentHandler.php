<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;

class AccumulatorsDepartmentHandler extends Handler
{
    public function resolve(FailureRequest $request)
    {
        if ($request->isSolarBattery()) {
            $this->setResolvedResponse(new ResolvedResponse(ResolvedResponse::ACCUMULATORS_DEPARTMENT_SOLUTION));
        }
    }
}
