<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;

abstract class Handler
{
    private $successor;

    private $resolvedResponse;

    abstract public function resolve(FailureRequest $request);

    public function setSuccessor(Handler $successor)
    {
        $this->successor = $successor;
    }

    protected function setResolvedResponse(ResolvedResponse $response)
    {
        $this->resolvedResponse = $response;
    }

    public function isResolved(): bool
    {
        return $this->resolvedResponse instanceof ResolvedResponse;
    }

    public function getResolvedResponse(): ResolvedResponse
    {
        return $this->resolvedResponse;
    }
}
