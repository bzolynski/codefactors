<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;

class CommunicationDepartmentHandler extends Handler
{
    public function resolve(FailureRequest $request)
    {
        if ($request->isTransmitter() || $request->isReceiver() || $request->isEncoder()) {
            $this->setResolvedResponse(new ResolvedResponse(ResolvedResponse::COMMUNICATION_DEPARTMENT_SOLUTION));
        }
    }
}
