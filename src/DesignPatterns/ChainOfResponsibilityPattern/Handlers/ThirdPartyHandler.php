<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;

class ThirdPartyHandler extends Handler
{
    const THIRD_PARTY_MANUFACTURERS = [
        'SpaceTek',
        'EarthTek',
        'OceanTek'
    ];

    public function resolve(FailureRequest $request)
    {
        if ($request->hasManufacturer()) {
            if (in_array($request->getManufacturer(), self::THIRD_PARTY_MANUFACTURERS)) {
                $this->setResolvedResponse(new ResolvedResponse(ResolvedResponse::THIRD_PARTY_SOLUTION));
            }
        }
    }
}
