Chain of Responsibility Pattern
===============================

Q: What to do, when there is more than one failure per unit?
It could happen then, that more than one department will be involved, so how to implement it
in the chain?

A: This pattern clearly tries to resolve the single issue against the chain of potential solutions.
Trying to find solution for multiple cases is problematic by nature. There could be more than one
solution per case, so we want to keep this complexity away. In fact, nothing stops us to keep each
failure as the separate request which we can easily resolve as the individual unit. Requests still
could have relation between each other if required, even we could have another chain, which checks
in the first place, how many failures are submitted per unit, and decides if they should be treated
individually or not, automatically or as the part of the manual process, etc.
