<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ObserverPattern;

use SplSubject;

interface NewsPublisherInterface extends SplSubject
{
    public function getMessage(): string;
}
