<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ObserverPattern\Repository;

/**
 * Simple in-memory repository just to imitate the concept of DB persistence
 */
class InMemoryRepository
{
    private $accounts = [];

    public function insert(string $account): void
    {
        $this->accounts[] = $account;
    }

    public function getAll(): array
    {
        return $this->accounts;
    }
}
