<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ObserverPattern\Subscribers;

use CODEfactors\DesignPatterns\ObserverPattern\NewsPublisherInterface;
use CODEfactors\DesignPatterns\ObserverPattern\Repository\InMemoryRepository;
use SplObserver;
use SplSubject;
use Exception;

class SmsSubscriber implements SplObserver
{
    private $sentSms = [];

    private $repository;

    public function __construct(InMemoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function update(SplSubject $subject)
    {
        if (!$subject instanceof NewsPublisherInterface) {
            // Seems like Standard PHP Library makes it more difficult,
            // so it's better just to create all interfaces by your own
            throw new Exception('This is the only way I can tell you that I really need this interface');
        }
        foreach ($this->getListOfSmsAccounts() as $smsAccount) {
            $this->sendSms($smsAccount, $subject->getMessage());
        }
    }

    public function getSentSms(): array
    {
        return $this->sentSms;
    }

    public function addSmsAccount(string $smsAccount): void
    {
        $this->repository->insert($smsAccount);
    }

    private function sendSms(string $phoneNumber, string $message): void
    {
        $this->sentSms[] = 'SMS with message "' . $message . '" sent to ' . $phoneNumber;
    }

    private function getListOfSmsAccounts(): array
    {
        return $this->repository->getAll();
    }
}
