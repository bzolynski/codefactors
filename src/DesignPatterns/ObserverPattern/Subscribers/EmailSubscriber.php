<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ObserverPattern\Subscribers;

use CODEfactors\DesignPatterns\ObserverPattern\NewsPublisherInterface;
use CODEfactors\DesignPatterns\ObserverPattern\Repository\InMemoryRepository;
use SplObserver;
use SplSubject;
use Exception;

class EmailSubscriber implements SplObserver
{
    private $sentEmails = [];

    private $repository;

    public function __construct(InMemoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function update(SplSubject $subject)
    {
        if (!$subject instanceof NewsPublisherInterface) {
            throw new Exception('This is the only way I can tell you that I really need this interface');
        }
        foreach ($this->getListOfEmailAccounts() as $emailAccount) {
            $this->sendEmail($emailAccount, $subject->getMessage());
        }
    }

    public function getSentEmails(): array
    {
        return $this->sentEmails;
    }

    public function addEmailAccount(string $emailAccount): void
    {
        $this->repository->insert($emailAccount);
    }

    private function sendEmail(string $emailAccount, string $message): void
    {
        $this->sentEmails[] = 'E-mail with message "' . $message . '" sent to ' . $emailAccount;
    }

    private function getListOfEmailAccounts(): array
    {
        return $this->repository->getAll();
    }
}
