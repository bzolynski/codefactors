<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ObserverPattern;

use SplObserver;

class NewsPublisher implements NewsPublisherInterface
{
    /**
     * @var SplObserver[]
     */
    private $subscribers = [];

    public function getMessage(): string
    {
        return 'This is a message taken from Database for today';
    }

    public function attach(SplObserver $subscriber): void
    {
        $this->subscribers[] = $subscriber;
    }

    public function detach(SplObserver $subscriber): void
    {
        foreach ($this->subscribers as $key => $aSubscriber) {
            if ($subscriber === $aSubscriber) {
                unset($this->subscribers[$key]);
            }
        }
    }

    /**
     * Push model used here as subjects send detailed data, whereas there is also a Pull model option
     * where the subject just notifies the observers when a change in his state appears and
     * it's the responsibility of each observer to pull the required data from the subject.
     */
    public function notify(): void
    {
        foreach ($this->subscribers as $subscriber) {
            $subscriber->update($this);
        }
    }
}
