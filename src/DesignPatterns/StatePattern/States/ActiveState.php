<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StatePattern\States;

class ActiveState extends State implements CanBeClosedInterface, CanBeBlockedInterface
{

}
