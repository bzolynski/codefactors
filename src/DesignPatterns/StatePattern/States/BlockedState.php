<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StatePattern\States;

class BlockedState extends State implements CanBeUnblockedInterface
{

}
