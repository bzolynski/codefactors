<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StatePattern\States;

class InitState extends State implements CanBeClosedInterface, CanBeActivatedInterface
{

}
