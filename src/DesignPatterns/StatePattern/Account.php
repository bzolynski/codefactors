<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\StatePattern;

use CODEfactors\DesignPatterns\StatePattern\States\ActiveState;
use CODEfactors\DesignPatterns\StatePattern\States\BlockedState;
use CODEfactors\DesignPatterns\StatePattern\States\CanBeBlockedInterface;
use CODEfactors\DesignPatterns\StatePattern\States\CanBeClosedInterface;
use CODEfactors\DesignPatterns\StatePattern\States\CanBeActivatedInterface;
use CODEfactors\DesignPatterns\StatePattern\States\CanBeUnblockedInterface;
use CODEfactors\DesignPatterns\StatePattern\States\ClosedState;
use CODEfactors\DesignPatterns\StatePattern\States\State;

use Exception;

class Account
{
    private $state;

    public function __construct(State $state)
    {
        $this->state = $state;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function activate()
    {
        if ($this->canBeActivated()) {
            $this->state = new ActiveState();
        } else {
            throw new Exception('Account cannot be confirmed in this state');
        }
    }

    public function block()
    {
        if ($this->canBeBlocked()) {
            $this->state = new BlockedState();
        } else {
            throw new Exception('Account cannot be closed in this state');
        }
    }

    public function unblock()
    {
        if ($this->canBeUnblocked()) {
            $this->state = new ActiveState();
        } else {
            throw new Exception('Account cannot be closed in this state');
        }
    }

    public function close()
    {
        if ($this->canBeClosed()) {
            $this->state = new ClosedState();
        } else {
            throw new Exception('Account cannot be closed in this state');
        }
    }

    private function canBeActivated(): bool
    {
        return $this->state instanceof CanBeActivatedInterface;
    }

    private function canBeClosed(): bool
    {
        return $this->state instanceof CanBeClosedInterface;
    }

    private function canBeBlocked(): bool
    {
        return $this->state instanceof CanBeBlockedInterface;
    }

    private function canBeUnblocked(): bool
    {
        return $this->state instanceof CanBeUnblockedInterface;
    }
}
