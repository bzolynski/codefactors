<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products;

class DomesticPackage extends Package
{
    public function addPriority(bool $isPriority): void
    {
        if ($isPriority) {
            $this->addCost(3);
            $this->setDeliveryDays(1);
        }
    }

    public function getDefaultDeliveryDays(): int
    {
        return 3;
    }

    public function getBaseCost(): float
    {
        return 2.5;
    }
}
