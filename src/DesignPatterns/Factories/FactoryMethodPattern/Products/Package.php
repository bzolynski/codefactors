<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products;

abstract class Package
{
    protected $orderNumber;

    protected $weight;

    protected $cost;

    private $maxDeliveryDays;

    public function __construct(int $weight)
    {
        $this->weight = $weight;
        $this->cost = $this->getBaseCost();
        $this->maxDeliveryDays = $this->getDefaultDeliveryDays();
    }

    public abstract function addPriority(bool $isPriority): void;

    public abstract function getDefaultDeliveryDays(): int;

    public abstract function getBaseCost(): float;

    public function isOverweighted(): bool
    {
        return $this->weight > $this->getMaxWeight();
    }

    public function getMaxWeight(): int
    {
        return 100;
    }

    public function getCost(): float
    {
        return $this->cost;
    }

    public function addCost(float $cost)
    {
        $this->cost += $cost;
    }

    public function getDeliveryDays(): int
    {
        return $this->maxDeliveryDays;
    }

    public function setOrderNumber(string $orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    protected function setDeliveryDays(int $maxDeliveryDays)
    {
        $this->maxDeliveryDays = $maxDeliveryDays;
    }
}
