<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products;

class InternationalPackage extends Package
{
    public function addPriority(bool $isPriority): void
    {
        if ($isPriority) {
            $this->addCost(5);
            $this->setDeliveryDays(7);
        }
    }

    public function getDefaultDeliveryDays(): int
    {
        return 10;
    }

    public function getMaxWeight(): int
    {
        return 75;
    }

    public function getBaseCost(): float
    {
        return 7;
    }
}
