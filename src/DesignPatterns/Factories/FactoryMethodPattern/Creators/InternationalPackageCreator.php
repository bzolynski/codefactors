<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Creators;

use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\InternationalPackage;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\Package;

class InternationalPackageCreator extends PackageCreator
{
    protected function createPackage(int $weight): Package
    {
        return new InternationalPackage($weight);
    }

    protected function setRuleOnWeightOverflow(Package $package): void
    {
        $package->addCost(5);
    }

    protected function generateNewOrderNumber(): string
    {
        return uniqid('INT');
    }
}
