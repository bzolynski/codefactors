<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Creators;

use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\DomesticPackage;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\Package;

class DomesticPackageCreator extends PackageCreator
{
    protected function createPackage(int $weight): Package
    {
        return new DomesticPackage($weight);
    }

    protected function setRuleOnWeightOverflow(Package $package): void
    {
        $package->addCost(2);
    }

    protected function generateNewOrderNumber(): string
    {
        return uniqid('DOM');
    }
}
