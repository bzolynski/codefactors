<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern;

use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\ClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\StandardClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\FreeMonthReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\Reward;

class SilverAccount extends Account
{
    public function getClubPoints(): ClubPoints
    {
        return new StandardClubPoints();
    }

    public function getReward(): Reward
    {
        return new FreeMonthReward();
    }
}
