<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern;

use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\NoReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\Reward;

class BronzeAccount extends Account
{
    public function getReward(): Reward
    {
        return new NoReward();
    }
}
