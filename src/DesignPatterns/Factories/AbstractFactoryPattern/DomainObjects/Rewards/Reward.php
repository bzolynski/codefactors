<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards;

abstract class Reward
{
    public function get(): string
    {
        return '';
    }


    public function apply()
    {

    }
}
