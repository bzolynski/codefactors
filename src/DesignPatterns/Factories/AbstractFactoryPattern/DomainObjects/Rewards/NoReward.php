<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards;

class NoReward extends Reward
{
    public function get(): string
    {
        return 'No rewards, sorry.';
    }
}
