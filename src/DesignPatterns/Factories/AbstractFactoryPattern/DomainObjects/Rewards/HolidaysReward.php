<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards;

class HolidaysReward extends Reward
{
    public function get(): string
    {
        return 'Holidays in New Zealand';
    }

    public function apply()
    {
        // Logic here
    }
}
