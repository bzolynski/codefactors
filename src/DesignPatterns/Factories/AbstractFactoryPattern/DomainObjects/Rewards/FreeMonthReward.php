<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards;

class FreeMonthReward extends Reward
{
    public function get(): string
    {
        return 'Free month reward';
    }

    public function apply()
    {
        // Logic here
    }
}
