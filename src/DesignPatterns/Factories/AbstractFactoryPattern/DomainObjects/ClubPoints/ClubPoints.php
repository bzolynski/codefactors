<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints;

abstract class ClubPoints
{
    public function getPoints(): int
    {
        return 0;
    }
}
