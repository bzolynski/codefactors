<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints;

class PremiumClubPoints extends ClubPoints
{
    public function getPoints(): int
    {
        return 100;
    }
}
