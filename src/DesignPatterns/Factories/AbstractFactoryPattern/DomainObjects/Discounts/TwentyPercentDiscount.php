<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts;

class TwentyPercentDiscount implements DiscountInterface
{
    public function getRate()
    {
        return 20;
    }

    public function warmUp()
    {
    }

    public function prepare()
    {
    }
}
