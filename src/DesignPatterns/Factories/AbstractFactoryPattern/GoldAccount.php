<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern;

use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\ClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\PremiumClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\TwentyPercentDiscount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\HolidaysReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\Reward;

class GoldAccount extends Account
{
    public function getClubPoints(): ClubPoints
    {
        return new PremiumClubPoints();
    }

    public function getReward(): Reward
    {
        return new HolidaysReward();
    }

    public function getDiscount(): DiscountInterface
    {
        return new TwentyPercentDiscount();
    }
}
