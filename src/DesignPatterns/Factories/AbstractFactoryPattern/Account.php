<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern;

use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\ClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\StandardClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\NoReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\Reward;

abstract class Account
{
    public function getClubPoints(): ClubPoints
    {
        return new StandardClubPoints();
    }

    public function getReward(): Reward
    {
        return new NoReward();
    }

    public function getDiscount(): DiscountInterface
    {
        return new TenPercentDiscount();
    }
}
