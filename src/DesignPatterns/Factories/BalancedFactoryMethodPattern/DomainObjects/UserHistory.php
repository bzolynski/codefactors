<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects;

class UserHistory
{
    public function isSubscribedMoreThanOneYear(): bool
    {
        return false;
    }
}
