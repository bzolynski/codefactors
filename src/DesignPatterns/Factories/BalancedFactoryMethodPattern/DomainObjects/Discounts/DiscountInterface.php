<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts;

interface DiscountInterface
{
    public function getRate();

    public function warmUp();

    public function prepare();
}
