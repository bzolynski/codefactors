<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern;

use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TwentyPercentDiscount;

class RandomDiscountProvider implements DiscountProvider
{
    public function getDiscount(): DiscountInterface
    {
        $random = rand(1, 2);
        if ($random === 1) {
            return new TenPercentDiscount();
        } else {
            return new TwentyPercentDiscount();
        }
    }
}
