<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern;

use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\DiscountInterface;

interface DiscountProvider
{
    public function getDiscount(): DiscountInterface;
}
