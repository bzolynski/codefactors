<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern;

use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TwentyPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\UserHistory;

class BalancedDiscountProvider implements DiscountProvider
{
    private $userHistory;

    public function __construct(UserHistory $userHistory)
    {
        $this->userHistory = $userHistory;
    }

    public function getDiscount(): DiscountInterface
    {
        if ($this->userHistory->isSubscribedMoreThanOneYear()) {
            return new TwentyPercentDiscount();
        } else {
            return new TenPercentDiscount();
        }
    }
}
