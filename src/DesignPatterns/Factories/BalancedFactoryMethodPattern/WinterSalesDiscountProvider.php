<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern;

use DateTime;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TwentyPercentDiscount;

class WinterSalesDiscountProvider implements DiscountProvider
{
    private $dateTime;

    public function __construct(DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    public function getDiscount(): DiscountInterface
    {
        if ($this->dateTime->format('d') > 24) {
            return new TwentyPercentDiscount();
        } else {
            return new TenPercentDiscount();
        }
    }
}
