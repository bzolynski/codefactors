<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\SimpleFactory;

use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\Discount;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\FiveOffDiscount;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\TwentyPercentDiscount;
use InvalidArgumentException;

/**
 * Not a design pattern itself, but can be used widely with any design pattern
 */
class DiscountFactory
{
    const TEN_PERCENT_DISCOUNT = '10_PRC';

    const TWENTY_PERCENT_DISCOUNT = '20_PRC';

    const FIVE_OFF_DISCOUNT = '5_OFF';

    public function create(string $discountType, float $price): Discount
    {
        switch ($discountType) {
            case self::TEN_PERCENT_DISCOUNT:
                return new TenPercentDiscount($price);
            case self::TWENTY_PERCENT_DISCOUNT:
                return new TwentyPercentDiscount($price);
            case self::FIVE_OFF_DISCOUNT:
                return new FiveOffDiscount($price);
            default:
                throw new InvalidArgumentException('Discount type not supported');
        }
    }
}
