<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\SimpleFactory\Products;

class TenPercentDiscount extends Discount
{
    public function getName(): string
    {
        return '10% discount!';
    }

    public function getValue(): float
    {
        return $this->price * 10 / 100;
    }
}
