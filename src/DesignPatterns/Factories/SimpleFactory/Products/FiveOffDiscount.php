<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\SimpleFactory\Products;

class FiveOffDiscount extends Discount
{
    public function getName(): string
    {
        if ($this->price < 5) {
            return 'Sorry, no discount...';
        } else {
            return '5 OFF discount!';
        }
    }

    public function getValue(): float
    {
        if ($this->price - 5 < 5) {
            return 0;
        }

        return 5;
    }
}
