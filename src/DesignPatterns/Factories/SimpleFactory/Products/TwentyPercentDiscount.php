<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\SimpleFactory\Products;

class TwentyPercentDiscount extends Discount
{
    public function getName(): string
    {
        return '20% discount!';
    }

    public function getValue(): float
    {
        return $this->price * 20 / 100;
    }
}
