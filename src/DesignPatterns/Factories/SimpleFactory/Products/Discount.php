<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\SimpleFactory\Products;

abstract class Discount
{
    protected $price;

    public function __construct(float $price)
    {
        $this->price = $price;
    }

    public abstract function getName(): string;

    public abstract function getValue(): float;
}
