<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\StaticFactory;

use CODEfactors\DesignPatterns\Factories\StaticFactory\Products\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\StaticFactory\Products\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\StaticFactory\Products\TwentyPercentDiscount;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class StaticFactory
{
    public static function create($discountPercentage): DiscountInterface
    {
        switch ($discountPercentage) {
            case 10:
                return new TenPercentDiscount();
            case 20:
                return new TwentyPercentDiscount();
            default:
                throw new InvalidArgumentException();
        }
    }
}
