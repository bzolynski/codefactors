<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\StaticFactory\Products;

class TwentyPercentDiscount implements DiscountInterface
{
    public function getRate()
    {
        return 20;
    }

    public function warmUp()
    {
    }

    public function prepare()
    {
    }
}
