<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\StaticFactory\Products;

interface DiscountInterface
{
    public function getRate();

    public function warmUp();

    public function prepare();
}
