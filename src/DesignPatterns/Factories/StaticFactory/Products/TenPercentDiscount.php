<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\Factories\StaticFactory\Products;

class TenPercentDiscount implements DiscountInterface
{
    public function getRate()
    {
        return 10;
    }

    public function warmUp()
    {
    }

    public function prepare()
    {
    }
}
