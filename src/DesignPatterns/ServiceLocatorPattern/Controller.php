<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ServiceLocatorPattern;

/**
 * Service Locator Pattern is very easy to implement,
 * but there's a good reason it is called an anti-pattern
 * as it hides dependencies and violates Tell, Don't Ask
 * principle which often can be fatal
 */
class Controller
{
    public function displayAccount(int $userId)
    {
        $account = ServiceLocator::getAccountService();
        return $account->getUserDataById($userId);
    }
}
