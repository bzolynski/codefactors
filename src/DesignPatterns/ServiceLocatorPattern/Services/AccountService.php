<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ServiceLocatorPattern\Services;

use CODEfactors\DesignPatterns\ServiceLocatorPattern\ServiceLocator;

class AccountService
{
    private $database;

    public function __construct()
    {
        $this->database = ServiceLocator::getDatabaseService();
    }

    public function getUserDataById(int $userId)
    {
        return $this->database->getUserById($userId);
    }
}
