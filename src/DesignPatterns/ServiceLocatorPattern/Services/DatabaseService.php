<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ServiceLocatorPattern\Services;

class DatabaseService
{
    private static $users = array(
        1 => array(
            'name' => 'John',
            'age' => 21
        ),
        2 => array(
            'name' => 'Sarah',
            'age' => 18
        )
    );

    public function getUserById(int $userId)
    {
        return self::$users[$userId];
    }
}
