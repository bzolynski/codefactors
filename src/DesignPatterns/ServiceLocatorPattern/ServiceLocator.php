<?php

declare(strict_types=1);

namespace CODEfactors\DesignPatterns\ServiceLocatorPattern;

use CODEfactors\DesignPatterns\ServiceLocatorPattern\Services\AccountService;
use CODEfactors\DesignPatterns\ServiceLocatorPattern\Services\DatabaseService;

class ServiceLocator
{
    private static $accountService;

    private static $databaseService;

    public static function getAccountService()
    {
        if (!self::$accountService) {
            self::$accountService = new AccountService();
        }
        return self::$accountService;
    }

    public static function getDatabaseService()
    {
        if (!self::$databaseService) {
            self::$databaseService = new DatabaseService();
        }
        return self::$databaseService;
    }
}
