<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Infrastructure\Persistence\InMemory;

use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model\User;
use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Repository\UserRepositoryInterface;
use Exception;

class InMemoryCloneUserRepository implements UserRepositoryInterface
{
    public $users = array();

    public function addUser(User $user): User
    {
        $this->users[] = clone $user;
        return clone $user;
    }

    public function getUserById(int $id): User
    {
        if (empty($this->users)) {
            throw new Exception('User not found');
        }
        foreach ($this->users as $user) {
            if ($user->getId() === $id) {
                return clone $user;
            }
        }
        throw new Exception('User not found');
    }

    public function activateByUserId(int $id): User
    {
        $user = $this->getUserById($id);
        $user->activate();
        /**
         * Even if this method below will be skipped, repository will be affected as reference to nested Status object
         * inside User is kept, and we want to have a separation between data stored in-memory and model representation
         */
        $this->persist($user);
        return clone $user;
    }

    private function persist(User $user): User
    {
        if (!empty($this->users)) {
            foreach ($this->users as $key => $aUser) {
                if ($aUser->getId() === $user->getId()) {
                    // Update
                    $this->users[$key] = clone $user;
                    return clone $user;
                }
            }
        }
        // Insert
        $this->users[] = clone $user;
        return clone $user;
    }
}
