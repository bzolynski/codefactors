<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Infrastructure\Persistence\InMemory;

use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model\User;
use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Repository\UserRepositoryInterface;
use Exception;

class InMemoryDeepCloneUserRepository implements UserRepositoryInterface
{
    public $users = array();

    public function addUser(User $user): User
    {
        $this->users[] = $this->deepClone($user);
        return $this->deepClone($user);
    }

    public function getUserById(int $id): User
    {
        if (empty($this->users)) {
            throw new Exception('User not found');
        }
        foreach ($this->users as $user) {
            if ($user->getId() === $id) {
                return $this->deepClone($user);
            }
        }
        throw new Exception('User not found');
    }

    public function activateByUserId(int $id): User
    {
        $user = $this->getUserById($id);
        $user->activate();
        $this->persist($user);
        return $this->deepClone($user);
    }

    private function persist(User $user): User
    {
        if (!empty($this->users)) {
            foreach ($this->users as $key => $aUser) {
                if ($aUser->getId() === $user->getId()) {
                    // Update
                    $this->users[$key] = $this->deepClone($user);
                    return $this->deepClone($user);
                }
            }
        }
        // Insert
        $this->users[] = $this->deepClone($user);
        return $this->deepClone($user);
    }

    private function deepClone(User $user): User
    {
        /**
         * This is a "real" clone, which will break all references also for nested objects
         */
        return unserialize(serialize($user));
    }
}
