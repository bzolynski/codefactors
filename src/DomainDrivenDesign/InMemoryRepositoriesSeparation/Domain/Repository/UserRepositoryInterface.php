<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Repository;

use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model\User;

interface UserRepositoryInterface
{
    public function addUser(User $user): User;

    public function getUserById(int $id): User;

    public function activateByUserId(int $id): User;
}
