<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model;

class Status
{
    const NOT_ACTIVATED = 1;

    const ACTIVATED = 2;

    private $value = self::NOT_ACTIVATED;

    public function activate(): void
    {
        $this->value = self::ACTIVATED;
    }

    public function isActivated(): bool
    {
        return $this->value === self::ACTIVATED;
    }
}
