<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model;

class User
{
    private $id = 0;

    private $status;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->status = new Status();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function activate(): void
    {
        $this->status->activate();
    }

    public function isActivated(): bool
    {
        return $this->status->isActivated();
    }
}
