In-memory Repositories Separation
=================================

What is the motivation to break the reference at all, and separate object stored in repository from the same objects
used in Domain, or Application layer? Regarding the nature of repositories, and the fact that they are separated
from model representation in code, the problem is that by manipulating the object on code level, you're affecting
the state of the object stored in repository, yet without persistence. It may introduce issues as some business
logic won't work the expected way when database or other implementation of repository will be put in place instead
of in-memory repository.

In-memory repositories are useful to focus on the domain design, and helps to test the business logic much faster
compare to database development implementation. It's true that DDD promotes persistence ignorance, and as far as our
in-memory repository belongs to Infrastructure Layer, it simulates the behaviour of the final implementation. Thus,
is worth to fully adapt the nature of the repository we're going to put in place at the end.

This implementation shows also that to achieve this goal, we have to do deep copy of objects instead of shallow copy.
Tests show the problem.
