<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model;

class Cargo
{
    private $weight;

    private $destinationPoint;

    public function __construct(int $weight, string $destinationPoint)
    {
        $this->weight = $weight;
        $this->destinationPoint = $destinationPoint;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getDestinationPoint(): string
    {
        return $this->destinationPoint;
    }
}
