<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model;

use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Exception\ShipOverflowException;

class Dock
{
    private $ship;

    private $cargoLeftInDock = array();

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function loadCargo(array $cargoList)
    {
        foreach ($cargoList as $cargo) {
            /** @var Cargo $cargo */
            try {
                $this->ship->bookCargo($cargo);
            } catch (ShipOverflowException $e) {
                $this->leaveCargoinDock($cargo);
            }
        }
    }

    public function getCargoListLeftInDock(): array
    {
        return $this->cargoLeftInDock;
    }

    private function leaveCargoInDock(Cargo $cargo)
    {
        $this->cargoLeftInDock[] = $cargo;
    }
}
