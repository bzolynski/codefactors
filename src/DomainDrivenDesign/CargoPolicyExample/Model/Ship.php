<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model;

use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Exception\ShipOverflowException;
use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Policy\CargoPolicy;

class Ship
{
    private $cargoWeight;

    const MAXIMUM_CARGO_WEIGHT = 1000;

    public function __construct()
    {
        $this->cargoWeight = 0;
    }

    public function bookCargo(Cargo $cargo)
    {
        if (CargoPolicy::isOverbooked($this, $cargo)) {
            throw new ShipOverflowException('Overbooked!');
        }
        $this->cargoWeight += $cargo->getWeight();
    }

    public function getMaximumCargoWeight(): int
    {
        return self::MAXIMUM_CARGO_WEIGHT;
    }

    public function getCargoWeight(): int
    {
        return $this->cargoWeight;
    }
}
