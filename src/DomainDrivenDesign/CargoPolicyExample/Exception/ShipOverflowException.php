<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\CargoPolicyExample\Exception;

use Exception;

class ShipOverflowException extends Exception
{

}
