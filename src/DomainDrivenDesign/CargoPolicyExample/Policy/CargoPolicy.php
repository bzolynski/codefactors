<?php

declare(strict_types=1);

namespace CODEfactors\DomainDrivenDesign\CargoPolicyExample\Policy;

use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model\Ship;
use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model\Cargo;

class CargoPolicy
{
    const MAX_CARGO_WEIGHT_PERCENTAGE = 1.1;

    public static function isOverbooked(Ship $ship, Cargo $cargo): bool
    {
        if ($ship->getCargoWeight() + $cargo->getWeight() >
            $ship->getMaximumCargoWeight() * self::MAX_CARGO_WEIGHT_PERCENTAGE) {
            return true;
        }

        return false;
    }
}
