<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DesignPatterns;

use CODEfactors\DesignPatterns\CommandPattern\Account;
use CODEfactors\DesignPatterns\CommandPattern\Command\CloseTransaction;
use CODEfactors\DesignPatterns\CommandPattern\Command\OpenTransaction;
use CODEfactors\DesignPatterns\CommandPattern\Command\Transfer;
use CODEfactors\DesignPatterns\CommandPattern\Invoker;
use PHPUnit\Framework\TestCase;

class InvokerTest extends TestCase
{
    const DISPLAY = false;

    public function testSingleCommandsRun()
    {
        $value = 5;
        $account = new Account();
        $invoker = new Invoker();
        $invoker->run(new OpenTransaction($account));
        $invoker->run(new Transfer($account, $value));
        $invoker->run(new CloseTransaction($account));
        $this->assertSame(3, $invoker->getCommandsCount());
        $this->assertSame($account->getAmount(), 5);
        $this->assertSame(3, count($invoker->getCommandsLog()));

        $this->displayResult($invoker->getCommandsLog());
    }

    public function testStackRun()
    {
        $value = 5;
        $account = new Account();
        $invoker = new Invoker();
        $invoker->addCommandToStack(new OpenTransaction($account));
        $invoker->addCommandToStack(new Transfer($account, $value));
        $invoker->addCommandToStack(new CloseTransaction($account));
        $this->assertSame(0, $invoker->getCommandsCount());

        $invoker->runStack();
        $this->assertSame(3, $invoker->getCommandsCount());
        $this->assertSame($account->getAmount(), 5);
        $this->assertSame(3, count($invoker->getCommandsLog()));

        $this->displayResult($invoker->getCommandsLog());
    }

    private function displayResult(array $commandsLog)
    {
        if (!self::DISPLAY) {
            return;
        }

        echo debug_backtrace()[1]['function'] . PHP_EOL;
        foreach ($commandsLog as $commandLog)
        {
            echo $commandLog . PHP_EOL;
        }
        echo PHP_EOL;
    }
}
