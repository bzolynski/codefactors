<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\ServiceLocatorPattern;

use CODEfactors\DesignPatterns\ServiceLocatorPattern\Controller;
use PHPUnit\Framework\TestCase;

class ServiceLocatorPatternTest extends TestCase
{
    public function test()
    {
        $data = array(
            'name' => 'John',
            'age' => 21
        );
        $controller = new Controller();
        $this->assertEquals($data, $controller->displayAccount(1));
    }
}
