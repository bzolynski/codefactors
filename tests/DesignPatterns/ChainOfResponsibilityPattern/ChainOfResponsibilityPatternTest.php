<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns;

use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\FailureRequest;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Dto\ResolvedResponse;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\FailureResolver;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers\AccumulatorsDepartmentHandler;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers\CommunicationDepartmentHandler;
use CODEfactors\DesignPatterns\ChainOfResponsibilityPattern\Handlers\ThirdPartyHandler;
use PHPUnit\Framework\TestCase;
use Exception;

class ChainOfResponsibilityPatternTest extends TestCase
{
    /** @var FailureResolver */
    private $resolver;

    public function setUp()
    {
        $this->resolver = new FailureResolver();
        $this->resolver->addHandler(new ThirdPartyHandler());
        $this->resolver->addHandler(new CommunicationDepartmentHandler());
        $this->resolver->addHandler(new AccumulatorsDepartmentHandler());
    }

    public function testTransmitterFailureWithListedManufacturer()
    {
        $failureRequest = new FailureRequest();
        $failureRequest->ackTransmitter('SpaceTek');

        $this->assertSame(
            ResolvedResponse::THIRD_PARTY_SOLUTION,
            $this->resolver->resolve($failureRequest)->getCode()
        );
    }

    public function testTransmitterFailureWithUnlistedManufacturer()
    {
        $failureRequest = new FailureRequest();
        $failureRequest->ackTransmitter('Unlisted Manufacturer');

        $this->assertSame(
            ResolvedResponse::COMMUNICATION_DEPARTMENT_SOLUTION,
            $this->resolver->resolve($failureRequest)->getCode()
        );
    }

    public function testSolarBatteryFailure()
    {
        $failureRequest = new FailureRequest();
        $failureRequest->ackSolarBattery('Our company');

        $this->assertSame(
            ResolvedResponse::ACCUMULATORS_DEPARTMENT_SOLUTION,
            $this->resolver->resolve($failureRequest)->getCode()
        );
    }

    public function testUnknownFailure()
    {
        $this->expectException(Exception::class);
        $failureRequest = new FailureRequest();

        $this->resolver->resolve($failureRequest);
    }
}
