<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\ObserverPattern;

use CODEfactors\DesignPatterns\ObserverPattern\Repository\InMemoryRepository;
use CODEfactors\DesignPatterns\ObserverPattern\Subscribers\EmailSubscriber;
use CODEfactors\DesignPatterns\ObserverPattern\Subscribers\SmsSubscriber;
use CODEfactors\DesignPatterns\ObserverPattern\NewsPublisher;
use PHPUnit\Framework\TestCase;

class ObserverPatternTest extends TestCase
{
    /** @var SmsSubscriber */
    private $smsSubscriber;

    /** @var EmailSubscriber */
    private $emailSubscriber;

    public function setUp()
    {
        $smsRepository = new InMemoryRepository();
        $this->smsSubscriber = new SmsSubscriber($smsRepository);
        $this->smsSubscriber->addSmsAccount('0-700-500-200');
        $this->smsSubscriber->addSmsAccount('0-700-500-201');

        $emailRepository = new InMemoryRepository();
        $this->emailSubscriber = new EmailSubscriber($emailRepository);
        $this->emailSubscriber->addEmailAccount('my@email.com');
    }

    public function testMessagePublishing()
    {
        $publisher = new NewsPublisher();
        $publisher->attach($this->smsSubscriber);
        $publisher->attach($this->emailSubscriber);
        $publisher->notify();

        $sentSms = $this->smsSubscriber->getSentSms();
        $this->assertSame(
            'SMS with message "This is a message taken from Database for today" sent to 0-700-500-200',
            $sentSms[0]
        );
        $this->assertSame(
            'SMS with message "This is a message taken from Database for today" sent to 0-700-500-201',
            $sentSms[1]
        );

        $sentEmails = $this->emailSubscriber->getSentEmails();
        $this->assertSame(
            'E-mail with message "This is a message taken from Database for today" sent to my@email.com',
            $sentEmails[0]
        );
    }
}
