<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DesignPatterns;

use CODEfactors\DesignPatterns\DecoratorPattern\Decorators\VatIncludedDecorator;
use CODEfactors\DesignPatterns\DecoratorPattern\Decorators\WholesaleDecorator;
use CODEfactors\DesignPatterns\DecoratorPattern\Products\BasicProduct;
use PHPUnit\Framework\TestCase;

class DecoratorPatternTest extends TestCase
{
    public function testCataloguePrice()
    {
        $product = new BasicProduct();
        $this->assertEquals(150.00, $product->price());
    }

    public function testShopPrice()
    {
        $product = new BasicProduct();
        $vatIncludedDecorator = new VatIncludedDecorator($product);
        $this->assertEquals(185.0, $vatIncludedDecorator->price());
    }

    public function testWholesalePriceWithoutVat()
    {
        $product = new BasicProduct();
        $wholesaleDecorator = new WholesaleDecorator($product, 1000);
        $this->assertEquals(148.00, $wholesaleDecorator->price());
    }

    public function testWholesalePriceWithVat()
    {
        $product = new BasicProduct();
        $vatIncludedDecorator = new VatIncludedDecorator($product);
        $wholesaleDecorator = new WholesaleDecorator($vatIncludedDecorator, 1000);
        $this->assertEquals(183.00, $wholesaleDecorator->price());
    }

    public function testWholesalePriceWithVatInDifferentOrder()
    {
        $product = new BasicProduct();
        $wholesaleDecorator = new WholesaleDecorator($product, 1000);
        $vatIncludedDecorator = new VatIncludedDecorator($wholesaleDecorator);
        $this->assertEquals(183.00, $vatIncludedDecorator->price());
    }
}
