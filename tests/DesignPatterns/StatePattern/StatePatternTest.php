<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\StatePattern\Account;
use CODEfactors\DesignPatterns\StatePattern\States\ActiveState;
use CODEfactors\DesignPatterns\StatePattern\States\BlockedState;
use CODEfactors\DesignPatterns\StatePattern\States\ClosedState;
use CODEfactors\DesignPatterns\StatePattern\States\InitState;
use PHPUnit\Framework\TestCase;
use Exception;

class StatePatternTest extends TestCase
{
    public function testActivateInitiatedAccount()
    {
        $state = new InitState();
        $account = new Account($state);
        $account->activate();
        $this->assertInstanceOf(ActiveState::class, $account->getState());
    }

    public function testActivateBlockedAccount()
    {
        $this->expectException(Exception::class);
        $state = new BlockedState();
        $account = new Account($state);
        $account->activate();
    }

    public function testCloseActivatedAccount()
    {
        $state = new ActiveState();
        $account = new Account($state);
        $account->close();
        $this->assertInstanceOf(ClosedState::class, $account->getState());
    }

    public function testCloseInitiatedAccount()
    {
        $state = new InitState();
        $account = new Account($state);
        $account->close();
        $this->assertInstanceOf(ClosedState::class, $account->getState());
    }

    public function testCloseBlockedAccount()
    {
        $this->expectException(Exception::class);
        $state = new BlockedState();
        $account = new Account($state);
        $account->close();
    }

    public function testBlockActivatedAccount()
    {
        $state = new ActiveState();
        $account = new Account($state);
        $account->block();
        $this->assertInstanceOf(BlockedState::class, $account->getState());
    }

    public function testBlockInitiatedAccount()
    {
        $this->expectException(Exception::class);
        $state = new InitState();
        $account = new Account($state);
        $account->block();
    }

    public function testBlockClosedAccount()
    {
        $this->expectException(Exception::class);
        $state = new ClosedState();
        $account = new Account($state);
        $account->block();
    }

    public function testUnblockBlockedAccount()
    {
        $state = new BlockedState();
        $account = new Account($state);
        $account->unblock();
        $this->assertInstanceOf(ActiveState::class, $account->getState());
    }

    public function testUnblockInitiatedAccount()
    {
        $this->expectException(Exception::class);
        $state = new InitState();
        $account = new Account($state);
        $account->unblock();
    }

    public function testUnblockActivatedAccount()
    {
        $this->expectException(Exception::class);
        $state = new ActiveState();
        $account = new Account($state);
        $account->unblock();
    }

    public function testUnblockClosedAccount()
    {
        $this->expectException(Exception::class);
        $state = new ClosedState();
        $account = new Account($state);
        $account->unblock();
    }
}
