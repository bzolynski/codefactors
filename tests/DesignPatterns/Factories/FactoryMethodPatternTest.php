<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\Package;
use PHPUnit\Framework\TestCase;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Creators\DomesticPackageCreator;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Creators\InternationalPackageCreator;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\DomesticPackage;
use CODEfactors\DesignPatterns\Factories\FactoryMethodPattern\Products\InternationalPackage;

class FactoryMethodPatternTest extends TestCase
{
    public function testDomesticPackageFactory()
    {
        $factory = new DomesticPackageCreator();
        $package = $factory->create(20);
        $this->assertInstanceOf(DomesticPackage::class, $package);
        $this->assertSame('DOM', $this->getOrderNumberPrefix($package));
        $this->assertSame(3, $package->getDeliveryDays());
        $this->assertSame(100, $package->getMaxWeight());
        $this->assertSame(2.5, $package->getBaseCost());
        $this->assertSame(2.5, $package->getCost());
    }

    public function testPriorityDomesticPackageFactory()
    {
        $factory = new DomesticPackageCreator();
        $package = $factory->create(20, true);
        $this->assertInstanceOf(DomesticPackage::class, $package);
        $this->assertSame('DOM', $this->getOrderNumberPrefix($package));
        $this->assertSame(1, $package->getDeliveryDays());
        $this->assertSame(100, $package->getMaxWeight());
        $this->assertSame(2.5, $package->getBaseCost());
        $this->assertSame(5.5, $package->getCost());
    }

    public function testDomesticPackageFactoryWithOverflow()
    {
        $factory = new DomesticPackageCreator();
        $package = $factory->create(120);
        $this->assertInstanceOf(DomesticPackage::class, $package);
        $this->assertSame('DOM', $this->getOrderNumberPrefix($package));
        $this->assertSame(3, $package->getDeliveryDays());
        $this->assertSame(100, $package->getMaxWeight());
        $this->assertSame(2.5, $package->getBaseCost());
        $this->assertSame(4.5, $package->getCost());
    }

    public function testInternationalPackageFactory()
    {
        $factory = new InternationalPackageCreator();
        $package = $factory->create(10);
        $this->assertInstanceOf(InternationalPackage::class, $package);
        $this->assertSame('INT', $this->getOrderNumberPrefix($package));
        $this->assertSame(10, $package->getDeliveryDays());
        $this->assertSame(75, $package->getMaxWeight());
        $this->assertSame(7.0, $package->getBaseCost());
        $this->assertSame(7.0, $package->getCost());
    }

    public function testPriorityInternationalPackageFactory()
    {
        $factory = new InternationalPackageCreator();
        $package = $factory->create(10, true);
        $this->assertInstanceOf(InternationalPackage::class, $package);
        $this->assertSame('INT', $this->getOrderNumberPrefix($package));
        $this->assertSame(7, $package->getDeliveryDays());
        $this->assertSame(75, $package->getMaxWeight());
        $this->assertSame(7.0, $package->getBaseCost());
        $this->assertSame(12.0, $package->getCost());
    }

    public function testInternationalPackageFactoryWithOverflow()
    {
        $factory = new InternationalPackageCreator();
        $package = $factory->create(80);
        $this->assertInstanceOf(InternationalPackage::class, $package);
        $this->assertSame('INT', $this->getOrderNumberPrefix($package));
        $this->assertSame(10, $package->getDeliveryDays());
        $this->assertSame(75, $package->getMaxWeight());
        $this->assertSame(7.0, $package->getBaseCost());
        $this->assertSame(12.0, $package->getCost());
    }

    private function getOrderNumberPrefix(Package $package): string
    {
        return substr($package->getOrderNumber(), 0, 3);
    }
}
