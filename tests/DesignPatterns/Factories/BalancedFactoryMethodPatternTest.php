<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\Factories;

use DateTime;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\RandomDiscountProvider;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\WinterSalesDiscountProvider;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\Discounts\TwentyPercentDiscount;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\DomainObjects\UserHistory;
use CODEfactors\DesignPatterns\Factories\BalancedFactoryMethodPattern\BalancedDiscountProvider;
use PHPUnit\Framework\TestCase;

class BalancedFactoryMethodPatternTest extends TestCase
{
    public function testBalancedFactoryForUserSubscribedLessThanOneYear()
    {
        $userHistoryStub = $this->createMock(UserHistory::class);
        $userHistoryStub->method('isSubscribedMoreThanOneYear')->willReturn(false);
        $factory = new BalancedDiscountProvider($userHistoryStub);
        $this->assertInstanceOf(TenPercentDiscount::class, $factory->getDiscount());
    }

    public function testBalancedFactoryForUserSubscribedMoreThanOneYear()
    {
        $userHistoryStub = $this->createMock(UserHistory::class);
        $userHistoryStub->method('isSubscribedMoreThanOneYear')->willReturn(true);
        $factory = new BalancedDiscountProvider($userHistoryStub);
        $this->assertInstanceOf(TwentyPercentDiscount::class, $factory->getDiscount());
    }

    public function testRandomFactory()
    {
        $factory = new RandomDiscountProvider();
        $this->assertInstanceOf(DiscountInterface::class, $factory->getDiscount());
    }

    public function testWinterSalesFactoryForTenPercentDiscount()
    {
        $dateTime = new DateTime('2017-10-10');
        $factory = new WinterSalesDiscountProvider($dateTime);
        $this->assertInstanceOf(TenPercentDiscount::class, $factory->getDiscount());
    }

    public function testWinterSalesFactoryForTwentyPercentDiscount()
    {
        $dateTime = new DateTime('2017-12-25');
        $factory = new WinterSalesDiscountProvider($dateTime);
        $this->assertInstanceOf(TwentyPercentDiscount::class, $factory->getDiscount());
    }
}
