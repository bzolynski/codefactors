<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\Factories\SimpleFactory\DiscountFactory;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\FiveOffDiscount;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\SimpleFactory\Products\TwentyPercentDiscount;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class SimpleFactoryTest extends TestCase
{
    /** @var DiscountFactory */
    private $discountFactory;

    public function setUp()
    {
        $this->discountFactory = new DiscountFactory();
    }

    public function testTenPercentDiscount()
    {
        $discount = $this->discountFactory->create(DiscountFactory::TEN_PERCENT_DISCOUNT, 50);
        $this->assertInstanceOf(TenPercentDiscount::class, $discount);
        $this->assertSame('10% discount!', $discount->getName());
        $this->assertSame(5.0, $discount->getValue());
    }

    public function testTwentyPercentDiscount()
    {
        $discount = $this->discountFactory->create(DiscountFactory::TWENTY_PERCENT_DISCOUNT, 50);
        $this->assertInstanceOf(TwentyPercentDiscount::class, $discount);
        $this->assertSame('20% discount!', $discount->getName());
        $this->assertSame(10.0, $discount->getValue());
    }

    public function testFiveOffDiscount()
    {
        $discount = $this->discountFactory->create(DiscountFactory::FIVE_OFF_DISCOUNT, 50);
        $this->assertInstanceOf(FiveOffDiscount::class, $discount);
        $this->assertSame('5 OFF discount!', $discount->getName());
        $this->assertSame(5.0, $discount->getValue());
    }

    public function testFiveOffDiscountForLowPrice()
    {
        $discount = $this->discountFactory->create(DiscountFactory::FIVE_OFF_DISCOUNT, 4);
        $this->assertInstanceOf(FiveOffDiscount::class, $discount);
        $this->assertSame('Sorry, no discount...', $discount->getName());
        $this->assertSame(0.0, $discount->getValue());
    }

    public function testInvalidDiscountType()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->discountFactory->create('NON_EXISTING_DISCOUNT_TYPE', 50);
    }
}
