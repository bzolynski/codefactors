<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\PremiumClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\ClubPoints\StandardClubPoints;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\TenPercentDiscount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Discounts\TwentyPercentDiscount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\FreeMonthReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\HolidaysReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\DomainObjects\Rewards\NoReward;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\BronzeAccount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\GoldAccount;
use CODEfactors\DesignPatterns\Factories\AbstractFactoryPattern\SilverAccount;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase
{
    public function testBronzeAccount()
    {
        $factory = new BronzeAccount();
        $this->assertInstanceOf(StandardClubPoints::class, $factory->getClubPoints());
        $this->assertInstanceOf(NoReward::class, $factory->getReward());
        $this->assertInstanceOf(TenPercentDiscount::class, $factory->getDiscount());
    }

    public function testSilverAccount()
    {
        $factory = new SilverAccount();
        $this->assertInstanceOf(StandardClubPoints::class, $factory->getClubPoints());
        $this->assertInstanceOf(FreeMonthReward::class, $factory->getReward());
        $this->assertInstanceOf(TenPercentDiscount::class, $factory->getDiscount());
    }

    public function testGoldAccount()
    {
        $factory = new GoldAccount();
        $this->assertInstanceOf(PremiumClubPoints::class, $factory->getClubPoints());
        $this->assertInstanceOf(HolidaysReward::class, $factory->getReward());
        $this->assertInstanceOf(TwentyPercentDiscount::class, $factory->getDiscount());
    }
}
