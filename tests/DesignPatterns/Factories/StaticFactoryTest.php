<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\Factories\StaticFactory\Products\DiscountInterface;
use CODEfactors\DesignPatterns\Factories\StaticFactory\StaticFactory;
use PHPUnit\Framework\TestCase;

/**
 * Don't do that at home, and especially at work!
 * This example is only for educational purposes.
 */
class StaticFactoryTest extends TestCase
{
    /**
     * I'm not injectable as dependency, you can create object statically from any class
     */
    public function test()
    {
        $this->assertSame(10, StaticFactory::create(10)->getRate());
    }

    /**
     * I'm injectable now, but only when someone would instantiate me first.
     * My method is static still, which creates the drawback.
     * I can be used anywhere without being injected!
     * This design smells bad...
     */
    public function testWithDependencyInjection()
    {
        $staticFactory = new StaticFactory();
        $this->assertSame(10, $this->getDiscount($staticFactory, 10)->getRate());
    }

    private function getDiscount(StaticFactory $staticFactory, int $discountPercentage): DiscountInterface
    {
        return $staticFactory::create($discountPercentage);
    }
}
