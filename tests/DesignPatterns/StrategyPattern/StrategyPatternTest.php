<?php

declare(strict_types=1);

namespace Tests\CODEfactors\DesignPatterns\Factories;

use CODEfactors\DesignPatterns\StrategyPattern\CreditCardStrategy;
use CODEfactors\DesignPatterns\StrategyPattern\Item;
use CODEfactors\DesignPatterns\StrategyPattern\PayPalStrategy;
use CODEfactors\DesignPatterns\StrategyPattern\ShoppingCart;
use PHPUnit\Framework\TestCase;

class StrategyPatternTest extends TestCase
{
    /** @var ShoppingCart */
    private $shoppingCart;

    public function setUp()
    {
        $this->shoppingCart = new ShoppingCart();
        $this->shoppingCart->addItem(new Item('Matchbox racing car', 20));
        $this->shoppingCart->addItem(new Item('Matchbox fire car', 35));
    }

    public function testCreditCardPaymentConfirmation()
    {
        $creditCardStrategy = new CreditCardStrategy('My Name', '1234567890', '123', '2029-01-01');
        $this->assertSame(55, $this->shoppingCart->getAmount());
        $this->assertSame('Credit card payment for EUR 55', $this->shoppingCart->pay($creditCardStrategy));
    }

    public function testPayPalPaymentConfirmation()
    {
        $payPalPaymentStrategy = new PayPalStrategy('my@email.com', 'pass123');
        $this->assertSame(55, $this->shoppingCart->getAmount());
        $this->assertSame('PayPal payment for EUR 55', $this->shoppingCart->pay($payPalPaymentStrategy));
    }
}
