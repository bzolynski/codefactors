<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DomainDrivenDesign\Example1;

use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model\Dock;
use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model\Ship;
use CODEfactors\DomainDrivenDesign\CargoPolicyExample\Model\Cargo;
use PHPUnit\Framework\TestCase;

class CargoLoadTest extends TestCase
{
    const DISPLAY = false;

    public function testOverbookedCargoLoad()
    {
        $ship = new Ship();

        $cargoList = array(
            new Cargo(100, 'Africa'),
            new Cargo(950, 'Antarctica'),
            new Cargo(100, 'America')
        );

        $dock = new Dock($ship);
        $dock->loadCargo($cargoList);

        /** @var Cargo[] $cargoLeftInPort */
        $cargoLeftInPort = $dock->getCargoListLeftInDock();

        $this->assertSame(1050, $ship->getCargoWeight());
        $this->assertSame(1, count($cargoLeftInPort));
        $this->assertSame('America', $cargoLeftInPort[0]->getDestinationPoint());

        $this->displayResult($ship->getCargoWeight(), $dock);
    }

    private function displayResult(int $shipCargoWeight, Dock $dock)
    {
        if (!self::DISPLAY) {
            return;
        }

        echo debug_backtrace()[1]['function'] . PHP_EOL;
        echo 'Cargo weight on the ship: ' . $shipCargoWeight . 't' . PHP_EOL;

        foreach ($dock->getCargoListLeftInDock() as $cargo) {
            /** @var Cargo $cargo */
            echo 'Cargo left in the dock: ' .
                '"' . $cargo->getDestinationPoint() . '", ' . $cargo->getWeight() . 't' . PHP_EOL;
        }
        echo PHP_EOL;
    }
}
