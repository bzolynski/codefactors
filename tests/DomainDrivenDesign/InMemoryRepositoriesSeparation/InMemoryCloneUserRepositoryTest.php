<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DomainDrivenDesign\BreakingReferencesForInMemoryRepositories;

use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model\User;
use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Infrastructure\Persistence\InMemory\InMemoryCloneUserRepository;
use PHPUnit\Framework\TestCase;

/**
 * This example demonstrates, that to separate object stored in repository from the same object manipulated in OOP,
 * cloning the object is not enough, as nested objects still can contain references, and cloning the object
 * won't break these references. It's happening because clone method performs shallow copying.
 */
class InMemoryCloneUserRepositoryTest extends TestCase
{
    public function testUnwantedBehaviour()
    {
        $userRepository = new InMemoryCloneUserRepository();

        $this->addUsersToRepository($userRepository);

        $user = $userRepository->getUserById(2);

        /**
         * Update user's state, as shown in this example, it will affect the state of this User object in repository
         * even though we're getting the cloned version of User object
         */
        $user->activate();

        /**
         * The method in line below (intentionally commented out) should be called to persist the change of the state,
         * but as this example shows, even if it's commented out, Status object nested in User object will be affected
         * in in-memory repository as original reference is kept even when User object is a cloned one, and this is
         * a good example of shallow copy on Status object
         */
        // $userRepository->activateByUserId(2);

        $user = $userRepository->getUserById(2);

        /**
         * User returned from repository shouldn't be activated, so this assertion should return false.
         * Repository should be separated from User object, which has been affected, but not persisted to repository.
         */
        $this->assertTrue($user->isActivated());
        $this->assertInstanceOf(User::class, $user);
    }

    private function addUsersToRepository(InMemoryCloneUserRepository $userRepository): void
    {
        for ($userId = 1; $userId < 3; $userId ++) {
            $user = new User($userId);
            $userRepository->addUser($user);
        }
    }
}
