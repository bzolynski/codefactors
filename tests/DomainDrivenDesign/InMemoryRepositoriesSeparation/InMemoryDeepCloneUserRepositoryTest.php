<?php

declare(strict_types=1);

namespace CODEfactors\Tests\DomainDrivenDesign\BreakingReferencesForInMemoryRepositories;

use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Domain\Model\User;
use CODEfactors\DomainDrivenDesign\InMemoryRepositoriesSeparation\Infrastructure\Persistence\InMemory\InMemoryDeepCloneUserRepository;
use PHPUnit\Framework\TestCase;

/**
 * This example shows how to separate in-memory repository from our model by breaking all references with
 * deep cloning of model objects which removes all references also from nested objects
 */
class InMemoryDeepCloneUserRepositoryTest extends TestCase
{
   public function test()
   {
       $userRepository = new InMemoryDeepCloneUserRepository();

       $this->addUsersToRepository($userRepository);

       $user = $userRepository->getUserById(2);
       $user->activate();

       /**
        * Now it works correctly, as the change of object won't affect the object kept in the repository
        * Let's check if reference is kept, should return false
        */
       $user = $userRepository->getUserById(2);
       $this->assertFalse($user->isActivated());

       /**
        * This method will change the state of User, and will persist the change to repository
        */
       $userRepository->activateByUserId(2);

       /**
        * Now, User taken from the repository is an activated one
        */
       $user = $userRepository->getUserById(2);
       $this->assertTrue($user->isActivated());
       $this->assertInstanceOf(User::class, $user);
   }

   private function addUsersToRepository(InMemoryDeepCloneUserRepository $userRepository)
   {
       for ($userId = 1; $userId < 3; $userId ++) {
           $user = new User($userId);
           $userRepository->addUser($user);
       }
   }
}
